
#ifndef _COG_UDPDATAPROVIDER_
#define _COG_UDPDATAPROVIDER_

#include <string>
#include <sstream>
#include <iostream>
#include <thread>
#include <functional>
#include <chrono>

#include <cogDataProvider.h>
#include "driver/DatagramSocket.h"
#include "../Utils/cogParser.h"

class TCogUDPDataProvider : public TCogDataProvider {
public:

    TCogUDPDataProvider() {
    }

    ~TCogUDPDataProvider() {
    }

    bool init(std::string initString) {
        auto param = TCogParser::getParams(initString);
        if (param["IP"] > "") {
            IP = param["IP"];
        }
        if (param["PORT"] > "") {
            int value = std::atoi(param["PORT"].c_str());
            if (value > 0) {
                port = value;
            }
        }
        if (param["PORT_LOCAL"] > "") {
            int value = std::atoi(param["PORT_LOCAL"].c_str());
            if (value > 0) {
                port_local = value;
            }
        }
        return true;
    }

    int write(std::istream& stream) {
        int result = 0;
        if (doExit) return 0;
        stream.seekg(0, stream.end);
        int length = (int)stream.tellg();
        stream.seekg(0, stream.beg);
        // -------------------------
        if (length > 0) {
            char * buffer = new char [length + 1];
            stream.read(buffer, length);
            result = sock->sendTo(buffer, length, IP.c_str());
            delete[] buffer;
        }
        return result;
    }

    int write(void * buffer, unsigned int size) {
        std::stringstream data;
        data.write((const char *) buffer, size);
        return write(data);
    }

    bool open(TCogGetBufferEvent rxEvent) {
        userRxEvent = rxEvent;
        sock = new DatagramSocket(port, port_local, IP.c_str(), true, true);
        thr = new std::thread([this] {
            this->readingLoop(); });

        return true;
    }

    bool close() {
        if ((sock != NULL)&&(thr != NULL)) {
            doExit = 1;
            sock->SocketClose();
            thr->join();
            delete sock;
            delete thr;
            sock = NULL;
            thr = NULL;
        }
        return true;
    }
private:
    bool doExit;
    std::thread * thr;
    DatagramSocket *sock;
    TCogGetBufferEvent userRxEvent;
    std::string IP = "127.0.0.1";
    int port = 7000;
    int port_local = 7001;

    bool readingLoop() {
        doExit = 0;
        char msg[2000];
        while (doExit == 0) {
            long size = sock->receive(msg, sizeof (msg));
            if (doExit == 0) {
                if (size > 0) {
                    if (userRxEvent) {
                        userRxEvent((void *) &msg, size);
                    } else {
                    }
                } else {
                    doExit = 1;
                    return false;
                }
            }
        }
        return true;
    }
};


#endif


