#ifndef _COG_PROTOCOL_CPRR
#define _COG_PROTOCOL_CPRR

#include <string>
#include <functional>
#include <iostream>
#include <ostream>
#include <sstream>

#include "protocolcmdList.h"

class TCogProtocolFrameCPRR : public TCogProtocolFrame {
private:
    TRadarPacket pkt;
    bool stxFlag = false;
    unsigned int rxSize = 0;
public:
    //    virtual int pack(void * buffer, unsigned int size) = 0;
    //    virtual int unpack(void * buffer, unsigned int size) = 0;

    int unpack(std::istream& in, std::function<void (void *, unsigned int) > frameEvent) {
        int pktCount = 0;
        unsigned char c;

        in.seekg(0, in.end);
        int length = (int)in.tellg();
        in.seekg(0, in.beg);

        while (!in.eof() && (length > 0)) {
            char * ptr = (char *) &pkt;
            in.read((char *) &c, 1);
            // std::cout << "in() : 0x" << std::hex << (unsigned int)c << std::endl;
            length--;
            if (rxSize < sizeof (TRadarHeaderMessage)) {
                stxFlag = false;
                ptr[rxSize++] = c;
            }
            if (rxSize == sizeof (TRadarHeaderMessage)) {
                if (pkt.hdr.preamble != RADAR_PKT_PREAMBLE) {
                    rxSize--;
                    for (unsigned int i = 0; i < rxSize; i++) {
                        ptr[i] = ptr[i + 1];
                    }
                } else {
                    // STX OK
                    stxFlag = true;
                }
            }
            if (stxFlag) {
                if (pkt.hdr.length > 2000) {
                    stxFlag = false;
                    rxSize = 0;
                } else {
                    unsigned int messageTailSize = sizeof (TRadarHeaderMessage) + pkt.hdr.length - rxSize;
                    unsigned int availableSize = (messageTailSize <= (unsigned int)length) ? messageTailSize : (unsigned int)length;
                    in.read(&ptr[rxSize], availableSize);
                    rxSize += availableSize;
                    messageTailSize -= availableSize;
                    if (messageTailSize == 0) {
                        if (frameEvent) {
                            frameEvent((void*) ptr, rxSize);
                        }
                        stxFlag = false;
                        rxSize = 0;
                        pktCount++;
                    }
                }
            }
        }
        return pktCount;
    }
};

class TCogProtocolCPRR { // : public TCogProtocol {
public:

    bool init(std::string initString) {
        return false;
    }

    TCogProtocolCPRR & reqGetVersion() {
        pkt.hdr.pktType = RADAR_PKT_REQUEST_ID;
        //
        pkt.message.request = RADAR_REQ_GETVER;
        messageSize = sizeof (pkt.message.request);
        return *this;
    }

    TCogProtocolCPRR & reqSetMode(bool enable, bool streamMode) {
        pkt.hdr.pktType = RADAR_PKT_MODE_ID;
        //
        pkt.message.mode.power = (enable) ? 1 : 0;
        pkt.message.mode.streaming = (streamMode) ? 1 : 0;
        messageSize = sizeof (pkt.message.mode);
        return *this;
    }

    TCogProtocolCPRR & reqSetPlatform(double velocity, double yaw_rate, bool forward) {
        pkt.hdr.pktType = RADAR_PKT_PLATFORM_ID;
        //
        pkt.message.platform.Velocity = velocity;
        pkt.message.platform.Velocity = velocity;
        pkt.message.platform.Velocity = velocity;
        messageSize = sizeof (pkt.message.platform);
        return *this;
    }

    //int pack(void * buffer, unsigned int size, std::ostream& out) {

    std::stringstream& pack(std::stringstream&& out) {
        return pack(out);
    }

    std::stringstream& pack(std::stringstream& out) {
        pkt.hdr.preamble = RADAR_PKT_PREAMBLE;
        pkt.hdr.length = messageSize;
        int pktSize = messageSize + sizeof (pkt.hdr);
        //out << "1"; out.put(0);
        out.write((const char *) &pkt, pktSize);
        //out << "1"; out.put(0);
        return out;
    }

    int unpack(std::istream& in, std::function<void (void *, unsigned int) > frameEvent) {
        return cprrFrame.unpack(in, frameEvent);
    }

private:
    int messageSize = 0;
    TRadarPacket pkt;
    TCogProtocolFrameCPRR cprrFrame;
};


#endif
