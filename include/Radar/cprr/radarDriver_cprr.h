#ifndef _COG_RADAR_CPRR_
#define _COG_RADAR_CPRR_
#include <thread>
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <iomanip> 

#include <cogRadar.h>
#include <cogDataProvider.h>
#include <cogProtocol.h>

#include <DataProvider/udpDataProvider.h>
#include "../cprr/protocol.h"

class TCogRadarCPRR : public TCogRadar {
private:
    TCogRadarInfo radarInfo;
    TCogUDPDataProvider defaultDataProvider;
    TCogDataProvider* dp;
    bool radarUp = false;
    TCogProtocolCPRR cprrProtocol;
    TCogFrameEvent userFrameDoneEvent = NULL;
    void * userArg = NULL;
    std::vector<TCogRadarTarget> radarTargets;
    uint32_t lastErrorCode = 0;
public:

    TCogRadarCPRR() {
        dp = &defaultDataProvider;
    };

    ~TCogRadarCPRR() {
    };

    TCogRadarInfo & getVersion() {
        return radarInfo;
    }

    bool setPlatformInfo(TCogRadarSetPlatform info) {
        return true;
    }

    bool start(std::string modeString, TCogFrameEvent frameDoneEvent, void * arg) {
        if (radarUp) {
            userFrameDoneEvent = frameDoneEvent;
            userArg = arg;
            bool streamMode = (modeString.find("STREAM") != std::string::npos);
            dp->write(cprrProtocol.reqSetMode(true, streamMode).pack(std::stringstream{}));
        } else {
            return false;
        }
        return true;
    }

    bool stop() {
        return dp->write(cprrProtocol.reqSetMode(false, false).pack(std::stringstream{})) > 0;
    }

    bool set(std::string cmdString) {
        return true;
    }

    std::string get(std::string cmdString) {
        return "";
    }

    bool setDataProvider(TCogDataProvider* dataProvider) {
        if (dataProvider) {
            dp = dataProvider;
        }
        return true;
    };

    bool open() {
        return open("");
    };

    bool open(std::string initString) {
        radarUp = false;

        dp->init(initString);
        bool dataProviderIsOk = dp->open([this](void * buffer, unsigned int size) {
            this->rxEvent(buffer, size); });

        if (dataProviderIsOk) {
            for (int i = 0; i < 3; i++) {
                if (radarUp) {
                    break;
                }
                dp->write(cprrProtocol.reqGetVersion().pack(std::stringstream{}));
                std::this_thread::sleep_for(chrono::seconds(1));
            }
        }
        if (radarUp == false) {
            dp->close();
        }

        return radarUp;
    }

    bool close() {
        dp->close();
        return true;
    }

    void rxEvent(void * buffer, unsigned int size) {
        std::stringstream rxData;
        rxData.write((const char *) buffer, size);
        rxEvent(rxData);
    }

    void rxEvent(std::istream& in) {
        cprrProtocol.unpack(in, [this](void * buffer, unsigned int size) {
            this->rxCmd((TRadarPacket *) buffer, size);
        });
    }

private:

    void rxCmd(TRadarPacket * pkt, unsigned int size) {
        static uint64_t frameid = -1;
        radarUp = true;
        if (pkt->hdr.pktType == RADAR_PKT_INFO_ID) {
            // GET VERSION 
            std::stringstream stream;
            radarInfo.HardVersion = std::to_string((unsigned long) pkt->message.verInfo.HardVersion);
            radarInfo.SoftVersion = std::to_string((unsigned long) pkt->message.verInfo.SoftVersion);
            stream << std::uppercase << std::setfill('0') << std::setw(sizeof(pkt->message.verInfo.SerialNumb)*2) << std::hex << (unsigned long)pkt->message.verInfo.SerialNumb;
            radarInfo.SerialNumb = stream.str();
        }

        if (pkt->hdr.pktType == RADAR_PKT_ERROR_ID) {
            // GET ERROR CODE
            lastErrorCode = pkt->message.error.Error;
            if (lastErrorCode)
                if (userFrameDoneEvent != NULL) {
                    std::vector<TCogRadarTarget> emptyTarget;
                    userFrameDoneEvent(0, 0, -1*(int64_t)lastErrorCode, 0.0, emptyTarget, userArg);
                }
        }

        if (pkt->hdr.pktType == RADAR_PKT_DATA_ID) {
            // DATA
            if (frameid != pkt->message.data.GrabNumber) {
                frameid = pkt->message.data.GrabNumber;
                if (userFrameDoneEvent != NULL) {
                    userFrameDoneEvent(pkt->message.data.TimeStamp, frameid, pkt->message.data.Status, pkt->message.data.Speed, radarTargets, userArg);
                }
                // new frame
                radarTargets.clear();
            }
            int targetNum = (size - sizeof (TRadarTargetParam) - sizeof (TRadarHeaderMessage)) / sizeof (TRadarTarget);
            for (int i = 0; i < targetNum; i++) {
                TRadarTarget * target = &pkt->message.data.Targets[i];
                radarTargets.push_back({
                    target->ObjID,
                    target->Range,
                    target->Azimuth,
                    target->LiveTime,
                    target->Rcs,
                    target->Xoord,
                    target->Xrate,
                    target->Xacceleration,
                    target->Ycoord,
                    target->Yrate,
                    target->Yacceleration
                });
            }
        }
    }
};


#endif
