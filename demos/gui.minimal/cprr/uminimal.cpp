
#include "sciter-x.h"
#include "sciter-x-graphics.hpp"
#include "sciter-x-window.hpp"
#include "sciter-om.h"
#include <radarsdk.h>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>

#include "sciter-win-main.cpp"


using namespace std;

//TCogRadarX cprr(COG_RADAR_CPRR); // CPRR RADAR 24GGz
TCogRadarX cprr(COG_RADAR_DEMO); // CPRR Emulator

bool doExit = false;

void radarStop() {
	cprr.stop();
	cprr.close();
	return;
}


static RECT wrc = { 100, 100, 1200, 1350 };

class frame : public sciter::window {
public:
	frame() : window(SW_TITLEBAR | SW_RESIZEABLE | SW_CONTROLS /*| SW_TOOL*/ | SW_MAIN /*| SW_ENABLE_DEBUG*/, wrc) {}

	SOM_PASSPORT_BEGIN(frame)
		SOM_FUNCS(SOM_FUNC(NeedClose))
		SOM_PASSPORT_END

		sciter::value   NeedClose() {
			doExit = true;
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			return sciter::value((int)0);
	}
};

#include "resources.cpp"

auto frameDone = [&](uint64_t TimeStamp, uint64_t GrabNumber, int64_t Status, double Speed, vector<TCogRadarTarget>& targets, void* arg) {
	if ((arg)&&(!doExit)) {
		if (((frame*)arg)->is_valid()) {
			sciter::dom::element root = ((frame*)arg)->get_root();
			try {
				stringstream frameInfo;
				frameInfo << "Time: " << std::fixed << std::setprecision(1) << (TimeStamp / 1000000.0) << " s, Frame #" << GrabNumber << " Speed: " << Speed << " Status : " << Status;
				root.call_function("SetStatusLine", sciter::value(frameInfo.str()));

				std::vector<sciter::value> tPoints;
				for (TCogRadarTarget tg : targets) {
					tPoints.push_back(sciter::value((float)tg.Xcoord));
					tPoints.push_back(sciter::value((float)tg.Ycoord));
				}
				if (tPoints.size() > 0) {
					sciter::value arrayOfTargets = sciter::value::make_array(UINT(tPoints.size()), &tPoints[0]);
					sciter::value numOfTarget = sciter::value((int)tPoints.size() / 2);
					root.call_function("SetPointsXY", numOfTarget, arrayOfTargets);
				}
			}
			catch (sciter::script_error& err) {
				std::cerr << err.what() << std::endl;
			}
		}
	}
};

bool radarStart(frame* arg) {
	if (cprr.open(TCogParser::configFile2Str("uminimal.cfg"))) {
		cprr.start("STREAM", frameDone, (void*)arg);
		return true;
	}
	return false;
}

int uimain(std::function<int()> run) {

	sciter::archive::instance().open(aux::elements_of(resources)); // bind resources[] (defined in "resources.cpp") with the archive

	frame* pwin = new frame();

	pwin->load(WSTR("this://app/monitoring.htm"));
	pwin->expand();

	sciter::dom::element root = pwin->get_root();

	try {
		new std::thread([&] {
			while (!doExit) {
				root.call_function("SetRadarStatus", sciter::value((int)0));
				if (radarStart(pwin)) {
					string radarVersion = " Version Hardware: " + cprr.getVersion().HardVersion + " Version Software: " + \
						cprr.getVersion().SoftVersion + " SN : " + cprr.getVersion().SerialNumb;

					root.call_function("SetRadarStatus", sciter::value((int)1));
					root.call_function("SetStatusText", sciter::value(radarVersion));
					break;
				}
				else {
					root.call_function("SetRadarStatus", sciter::value((int)-1));
					std::this_thread::sleep_for(std::chrono::milliseconds(1500));
				}
			}
			});
	}
	catch (sciter::script_error& err) {
		std::cerr << err.what() << std::endl;
	}

	run();
	doExit = true;
	radarStop();
	return 0;
}
